import subprocess
import os
import execute

def main():
    input_file  = os.path.dirname(os.path.realpath(__file__))+"/../input/BOV.vob"
    output_file = os.path.dirname(os.path.realpath(__file__))+"/../output/VOB.mp4"
    if not (os.path.isfile(output_file)):
        command =["ffmpeg","-i",input_file,output_file]
        execute.execute(command)

if __name__ == '__main__':
    main()
