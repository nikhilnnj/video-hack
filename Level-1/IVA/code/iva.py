import subprocess
import os
import glob
import time
import execute


def createInputFile():
    in_dir=os.path.dirname(os.path.realpath(__file__))+"/../input/"
    target_file = open(in_dir+"../output/input_list.txt",'w')
    target_file.truncate()
    date_abs_dir= glob.glob(os.path.dirname(os.path.realpath(__file__))+"/../input/IVA/*")
    date_name=[]
    for x in date_abs_dir:
        date_name.append(time.strptime(((x.split('/')[-1]).replace('.','/')), "%d/%m/%Y"))
    date_name.sort()
    for date in date_name:
        index_num=[]
        date_str=time.strftime("%d/%m/%Y",date).replace('/','.')
        index_dirs = glob.glob(os.path.dirname(os.path.realpath(__file__)) +
                               "/../input/IVA/"+date_str+"/*")
        for index_dir in index_dirs:
            index_num.append(index_dir.split('/')[-1])
        index_num.sort()
        for index in index_num:
            target_file.write("file \'"+in_dir+'IVA/'+date_str+'/'+index+'/archive.iva'+'\'\n')



def main():
    createInputFile()
    input_file  = os.path.dirname(os.path.realpath(__file__))+"/../output/input_list.txt"
    output_file_mp4 = os.path.dirname(os.path.realpath(__file__))+"/../output/MP4_output.mp4"
    output_file_mkv = os.path.dirname(os.path.realpath(__file__))+"/../output/MKV_output.mkv"
    if not (os.path.isfile(output_file_mkv)):
        command =["ffmpeg","-f","concat","-safe","0","-r","10","-i",input_file,output_file_mkv]
        execute.execute(command)
    if not (os.path.isfile(output_file_mp4)):
        command =["ffmpeg","-f","concat","-safe","0","-r","10","-i",input_file,output_file_mp4]
        execute.execute(command)


if __name__ == '__main__':
    main()
